<?php

namespace App\GraphQL\Types;

use App\Models\Author;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Rebing\GraphQL\Support\Facades\GraphQL;

class AuthorType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Author',
        'description' => 'Collection of Authors and details of Author',
        'model' => Author::class
    ];


    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id of a particular author',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the author',
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The email of the author',
            ],
            'password' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The password of the author',
            ],
            'age' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The age of the author',
            ],
            'api_token' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The api_token of the author',
            ],
            'books' => [
                'type' => Type::listOf(GraphQL::type('Book')),
                'description' => 'List of books'
            ]
        ];
    }
}
