<?php

namespace App\graphql\Queries;

use App\Models\Book;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use Tymon\JWTAuth\Facades\JWTAuth;

class BooksQuery extends Query
{
    private $auth;

    protected $attributes = [
        'name' => 'books',
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('Book'));
    }

    public function authenticated($root, $args, $currentUser)
    {
      return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        return Book::all();
    }
    
}
