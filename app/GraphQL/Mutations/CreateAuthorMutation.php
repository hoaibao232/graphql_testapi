<?php

namespace App\graphql\Mutations;

use App\Models\Author;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CreateAuthorMutation extends Mutation
{
    protected $attributes = [
        'name' => 'createAuthor'
    ];

    public function type(): Type
    {
        return GraphQL::type('Author');
    }

    public function args(): array
    {
        return [
            'name' => [
                'name' => 'name',
                'type' =>  Type::nonNull(Type::string()),
            ],
            'email' => [
                'name' => 'email',
                'type' =>  Type::nonNull(Type::string()),
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::nonNull(Type::string())
            ],
            'age' => [
                'name' => 'age',
                'type' =>  Type::nonNull(Type::int()),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $args['password'] = bcrypt($args['password']);
        $author = new Author();
        $author->fill($args);
        $author->save();

        return $author;
    }
}
