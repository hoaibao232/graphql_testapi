<?php

namespace App\graphql\Mutations;

use App\Models\Author;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class DeleteAuthorMutation extends Mutation
{
    protected $attributes = [
        'name' => 'deleteAuthor',
        'description' => 'Delete a Author'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }


    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $author = Author::findOrFail($args['id']);

        return  $author->delete() ? true : false;
    }
}
