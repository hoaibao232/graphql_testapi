<?php

namespace App\graphql\Mutations;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class LoginAuthorMutation extends Mutation
{
    protected $attributes = [
        'name' => 'loginAuthor'
    ];

    public function type(): Type
      {
        return Type::string();
      }

      public function args(): array
      {
        return [
          'email' => [
            'name' => 'email',
            'type' => Type::nonNull(Type::string()),
            'rules' => ['required', 'email'],
          ],
          'password' => [
            'name' => 'password',
            'type' => Type::nonNull(Type::string()),
            'rules' => ['required'],
          ],
        ];
      }

  public function resolve($root, $args)
  {

    $credentials = [
      'email' => $args['email'],
      'password' => $args['password']
    ];

    $checkLogin = Auth::guard('author')->attempt($credentials);

    if($checkLogin) {
        $author = Auth::guard('author')->user();
        
        $token = Str::random(60);
   

        $author->api_token = $token;
        $author->save();

        return $token;
    }

   return "wrong";

    // $token = Auth::guard('author')->attempt($credentials);

    //     if (!$token) {
    //       throw new \Exception('Unauthorized!');
    //     }

    //     return $token;
  }
}