<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Author extends Authenticatable
{
    use HasFactory, HasApiTokens;

    protected $guard = 'author';

    protected $fillable = [
        'name', 'email', 'age', 'password','api_token'
    ];

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
