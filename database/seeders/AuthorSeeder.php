<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Author::insert([
            [
                'name' => "Chimamanda Ngozi Adichie",
                'email' => "nguyenhoaibaoltt@gmail.com",
                'age' => 30,
                'password' => bcrypt('12345678')          
            ],
            [
                'name' => "William F. Williams",
                'email' => "Williams@gmail.com",
                'age' => 45,
                'password' => bcrypt('12345678')
            ],
            [
                'name' => "Chinua Achebe",
                'email' => "Chinua@gmail.com",
                'age' => 65,
                'password' => bcrypt('12345678')
            ],
           
        ]);
    }
}
